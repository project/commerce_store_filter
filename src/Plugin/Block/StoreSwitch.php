<?php
/**
 * @file
 * Contains \Drupal\commerce_store_filter\Plugin\Block\StoreSwitch
 * Purpose of this block is to display an Associate site address & Logo from Store Address of Domain
 *
 */
namespace Drupal\commerce_store_filter\Plugin\Block;


use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'StoreSwitch' block.
 *
 * @Block(
 *   id = "commerce_store_filter_block",
 *   admin_label = @Translation("Commerce Store Switch"),
 *
 * )
 */
class StoreSwitch extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\commerce_store_filter\Form\StoreSelectForm',
      [
        'display_store_label' => $this->configuration['display_store_label'],
        'display_store_currency' => $this->configuration['display_store_currency']
      ]);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['store_display_options'] = [
      '#type' => 'fieldset',
      '#title' => t('Store dropdown display options'),
    ];

    $form['store_display_options']['display_store_label'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the store label'),
      '#default_value' => $this->configuration['display_store_label'] ?? TRUE,
      '#weight' => '10',
    ];

    $form['store_display_options']['display_store_currency'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the store currency'),
      '#default_value' => $this->configuration['display_store_currency'] ?? TRUE,
      '#weight' => '11',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!$values['store_display_options']['display_store_label'] && !$values['store_display_options']['display_store_currency']) {
      $form_state->setErrorByName('store_display_options', $this->t('Please check one or both display options.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['display_store_label'] = $values['store_display_options']['display_store_label'];
    $this->configuration['display_store_currency'] = $values['store_display_options']['display_store_currency'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // @TODO could this be cached by store id & currency? Too risky?
    return 0;
  }

}
