<?php

namespace Drupal\commerce_store_filter;

use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Html;

/**
 * Class CommerceStoreFilterStoreService.
 *
 */
class CommerceStoreFilterStoreService {

  /**
   * EntityTypeManager.
   *
   * @var EntityTypeManager $entity_type_manager.
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Array of valid store ids.
   *
   * @var array
   */
  protected array $available_stores;

  /**
   * Drupal\Core\Messenger\Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected Messenger $messenger;


  /**
   * Drupal\Core\TempStore\PrivateTempStore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $sessionStore;

  /**
   * Constructs a new CommerceStoreFilterStoreService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManager $entity_type_manager, Messenger $messenger, PrivateTempStoreFactory $temp_store_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->sessionStore = $temp_store_factory->get('commerce_store_filter');
    // @TODO Is the below sufficent to filter or is unpublished an option?
    $this->available_stores = $this->entityTypeManager
      ->getStorage('commerce_store')
      ->loadMultiple();
  }

  /**
   * Set persona for current user.
   *
   * If they have a set store then we use it, if not then we do nothing
   * and leave the user with whatever their currently selected store is.
   * @param int $store_id
   *   System id for a commerce_store.
   */
  public function setCommerceStore(int $store_id) {
    if (array_key_exists($store_id, $this->available_stores)) {
      $this->sessionStore->set('commerce_store_filter_store', $store_id); // Saving the session for the access.
      // @TODO make this message customisable.
      $this->messenger->addMessage('Store has been switched successfully!');
    }
  }

  /**
   * Returns the user's current Commerce Store.
   */
  public function getCommerceStore() {
    if ($this->sessionStore->get('commerce_store_filter_store') && array_key_exists($this->sessionStore->get('commerce_store_filter_store'), $this->available_stores)) {
      return $this->entityTypeManager
        ->getStorage('commerce_store')
        ->load($this->sessionStore->get('commerce_store_filter_store'));
    }

    return $this->entityTypeManager->getStorage('commerce_store')->loadDefault();
  }

}
