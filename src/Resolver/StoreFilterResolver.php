<?php

namespace Drupal\commerce_store_filter\Resolver;

use Drupal\commerce_store\Resolver\StoreResolverInterface;
use Drupal\commerce_store_filter\CommerceStoreFilterStoreService;
use Symfony\Component\HttpFoundation\RequestStack;

class StoreFilterResolver implements StoreResolverInterface {

  /**
   * Drupal\commerce_store_filter\CommerceStoreFilterStoreService.
   *
   * @var \Drupal\commerce_store_filter\CommerceStoreFilterStoreService
   *
   */
  protected $csfStoreService;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new DefaultStoreResolver object.
   *
   * @param \Drupal\commerce_store_filter\CommerceStoreFilterStoreService $csf_store_service
   *   The CSF store service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(CommerceStoreFilterStoreService $csf_store_service, RequestStack $request_stack) {
    $this->csfStoreService = $csf_store_service;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function resolve() {
    $current_request = $this->requestStack->getCurrentRequest();
    if ($store_id = $current_request->query->get('commerce_store_filter')) {
      $this->csfStoreService->setCommerceStore($store_id);
      $current_request->query->remove('commerce_store_filter');
    }
    return $this->csfStoreService->getCommerceStore();
  }

}
