<?php

/**
 * @file
 * Contains \Drupal\commerce_store_filter\Form
 * Store selection form for multi-store
 *
 */
namespace Drupal\commerce_store_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Class StoreSelectForm for store selection.
*/
class StoreSelectForm extends FormBase {

  /**
   * Whether to display the store label.
   *
   * @var bool
   */
  protected $display_label;

  /**
   * Whether to display the store currency.
   *
   * @var bool
   */
  protected $display_currency;

  /**
   * {@inheritdoc}
  */
  public function getFormId() {
    return 'commerce_store_select_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->display_label = $form_state->getBuildInfo()['args'][0]['display_store_label'] ?? TRUE;
    $this->display_currency = $form_state->getBuildInfo()['args'][0]['display_store_currency'] ?? TRUE;

    $store_list = [];
    $stores = \Drupal::database()
      ->select('commerce_store_field_data', 'cs')
      ->fields('cs', ['store_id', 'name', 'default_currency'])
      ->execute()
      ->fetchAll();

    foreach ($stores as $value) {
      if ($this->display_label && $this->display_currency) {
        $store_list[$value->store_id] = $value->name . " (" . $value->default_currency . ")";
      }
      elseif ($this->display_label) {
        $store_list[$value->store_id] = $value->name;
      }
      else {
        $store_list[$value->store_id] = $value->default_currency;
      }
    }

    /** @var \Drupal\commerce_store_filter\CommerceStoreFilterStoreService $csf_store_service */
    $csf_store_service = \Drupal::service('commerce_store_filter.commerce_store_filter_store_service');
    $store = $csf_store_service->getCommerceStore();

    $form['store'] = [
      '#type' => 'select',
      '#title' => $this->t('Change Store'),
      '#options' => $store_list,
      '#default_value' => $store->id(),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Switch'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
  */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Nothing.
  }

  /**
   * {@inheritdoc}
  */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_store_filter\CommerceStoreFilterStoreService $csf_store_service */
    $csf_store_service = \Drupal::service('commerce_store_filter.commerce_store_filter_store_service');
    $csf_store_service->setCommerceStore($form_state->getValue('store'));
  }

}
