CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Commerce Store Switch module helps end-user to change the current store. 
The module provides a Drupal block, which helps user change the 
Commerce Store. You can also use query strings to provide links for your users in the format:

 * /?commerce_store_filter=[your_store_id]

Products map to specific Stores so after 
switching the store, Products related to the specific store selected are the only ones shown to and 
accessed by the users. 

This module will be providing a Contextual Filter for the Store filter into 
Drupal Views. The filter will help to add the condition to the Cart view 
and Order summary views.

REQUIREMENTS
------------
Drupal Commerce
Drupal Commerce Store
Views (Core)

INSTALLATION
------------
 * Install as usual:
 See https://www.drupal.org/documentation/install/modules-themes/modules-8
 for further information.

CONFIGURATION
-------------
There is no specific configuration for this module. Drupal Block for 
the Commerce Store Switch will be available after the module installation. 
The Views Contextual filter will available to filter.
